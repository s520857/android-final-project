package s520857.finalproject;

/**
 * Created by connorb on 11/17/17.
 */

public class Location {

    Double longitude;
    Double latitude;
    String typeofEncounter;
    String ticketType;
    String policeType;
    String comments;

    public Location () {

    }

    public Location (Double longitude, Double latitude, String typeofEncounter, String ticketType, String policeType, String comments) {

        this.longitude = longitude;
        this.latitude = latitude;

        // Types of encounters
        // Necessary to post
        this.typeofEncounter = typeofEncounter;

        // Necessary to post
        this.ticketType = ticketType;

        // Types of police
        // Necessary to post
        this.policeType = policeType;

        // Comments option and set to empty string
        this.comments = comments;
    }
}
