package s520857.finalproject;

import android.content.Context;
import android.content.Intent;
import android.location.LocationManager;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.widget.*;
import java.util.HashMap;
import java.util.Map;
import android.view.View;
import com.backendless.Backendless;
import com.backendless.async.callback.AsyncCallback;
import com.backendless.exceptions.BackendlessFault;

public class AddLocation extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_add_location);

        // Setting up backendless
        Backendless.setUrl(Defaults.SERVER_URL);
        Backendless.initApp(getApplicationContext(), Defaults.APPLICATION_ID, Defaults.API_KEY);

        Spinner spinner = (Spinner) findViewById(R.id.spinner);
        ArrayAdapter<CharSequence> adapter = ArrayAdapter.createFromResource(this, R.array.typeOfEncounter, android.R.layout.simple_spinner_item);
        adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        spinner.setAdapter(adapter);

        // Changes hint when spinner is changed
        spinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parentView, View selectedItemView, int position, long id) {

                EditText ticketTypeET = (EditText) findViewById(R.id.ticketTypeET);

                if (position == 0) {
                    ticketTypeET.setHint("Ticket type?");

                } else if (position == 1) {
                    ticketTypeET.setHint("Reason for warning?");

                } else if (position == 2) {
                    ticketTypeET.setHint("Type of crash?");

                } else {  // position == 3
                    ticketTypeET.setHint("Reason stopped?");
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> parentView) {
                return;
            }

        });
    }

    public void addTicket(View v) {

        // Initializing variables
        Double longitude;
        Double latitude;
        String typeofEncounter;
        String ticketType;
        String policeType;
        String comments;
        android.location.Location location;

        Spinner spinner = (Spinner) findViewById(R.id.spinner);
        EditText ticketTypeET = (EditText) findViewById(R.id.ticketTypeET);
        RadioGroup policeTypeRG = (RadioGroup) findViewById(R.id.radioGroup);
        RadioButton rb = (RadioButton) findViewById(policeTypeRG.getCheckedRadioButtonId());
        EditText commentsET = (EditText) findViewById(R.id.commentsET);

        // Getting longitude & latitude
        LocationManager lm = (LocationManager)getSystemService(Context.LOCATION_SERVICE);
        try {
            location = lm.getLastKnownLocation(LocationManager.GPS_PROVIDER);
            longitude = location.getLongitude();
            latitude = location.getLatitude();

        } catch (SecurityException e) {
            Toast.makeText(AddLocation.this, "We can't find you, try finding service!", Toast.LENGTH_LONG).show();
            return;
        }

        // Sets type of encounter
        typeofEncounter = spinner.getSelectedItem().toString();

        // Sets ticket type
        // If empty, displays toast message
        if (ticketTypeET.getText().toString().isEmpty()) {
            Toast.makeText(AddLocation.this, "You're missed a field.", Toast.LENGTH_LONG).show();
            return;
        } else {
            ticketType = ticketTypeET.getText().toString();
        }

        // Sets police type
        // If nothing has been selected, returns -1 so we send message and return
        if (policeTypeRG.getCheckedRadioButtonId() == -1) {
            Toast.makeText(AddLocation.this, "You're missed a field.", Toast.LENGTH_LONG).show();
            return;
        } else {
            policeType = rb.getText().toString();
        }

        // Sets comments
        // If no comments, sets to empty string
        // Comments aren't required
        if (commentsET.getText().toString().isEmpty()) {
            comments = "";
        } else {
            comments = commentsET.getText().toString();
        }

        Location tempLocation = new Location(longitude, latitude, typeofEncounter, ticketType, policeType, comments);

        // Storing in Backendless
        HashMap ticket = new HashMap<>();
        ticket.put("longitude", longitude);
        ticket.put("latitude", latitude);
        ticket.put("typeofEncounter", typeofEncounter);
        ticket.put("ticketType", ticketType);
        ticket.put("policeType", policeType);
        ticket.put("comments", comments);

        Backendless.Data.of("Locations").save(ticket, new AsyncCallback<Map>() {
            @Override
            public void handleResponse(Map response) {

                Toast.makeText(AddLocation.this, "Success!", Toast.LENGTH_LONG).show();
                finish();
            }

            @Override
            public void handleFault(BackendlessFault fault) {
                Toast.makeText(AddLocation.this, "Sorry, we could not post at this time.", Toast.LENGTH_LONG).show();
                Log.e( "MYAPP", "Server reported an error " + fault.getMessage() );
            }
        });
    }
}
