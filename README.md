# README #
Android Final Project
Devin Miller
Connor Besancenez

Our final project will be an app that will populate a map full of driving violations people have received. This will help lower the amount of tickets given due to people being educated on where common spots where moving violations are given like speeding tickets, rolling stops, etc.. This will cause people to be more mindful of police and common spots violations are given out which in turn will cause roads to be safer and police’s jobs to be safer.

This app will follow you along your route and will indicate heavily ticketed areas. If there is a way to populate previously given tickets from a database of tickets we will do that to start. If that is not available or possible we will start with a blank map and as people get tickets or moving violations the map will start to show trends along their routes or where they often drive of where tickets are given, where they need to be more careful, and highly populated areas.

Functionality
Storing data in a database
Internet
Maps
Location

Risky Components
Probably our greatest component will be the fact that we only have 2 people working on this project because our third partner dropped the class.
Finding data that we can populate the map with to begin with. Although the app doesn’t rely on previous data to show trends, it would be nice to have previous data so people load up the app and can start seeing previous trends instead of just building up the trends through inputting ticket they have received.
The trends of busy roads, heavily patrolled and ticketed areas change often so the data needs to have a time to live so it can cycle through old and new trends.
Getting people to enter where they received a ticket. We can combat this by advertising that it will make roads safer for drivers, will make roads less populated by people using alternative routes and will make police officer’s jobs safer. Many positive components come from people just inputting where they were pulled over ticketed, and for what reason.
We have to use google maps which will be difficult to keep an accurate representation of where the car is on the maps. Keeping it accurate while using as little data as possible will be a challenge.
Dealing with internet and location is always difficult.
Storing locations of where people had tickets given to place on the map in a database. Finding the quickest, cheapest (hopefully free), and best option can sometimes be difficult and a matter of opinion. 
People inputting their tickets will be difficult because people might be hesitant to give that information, fear they are being tracked, not understanding the idea behind it and all the positives that will come from it, or many other reasons. We will combat this by not tracking any of their information, they will not have any login information, and all their inputs into the system will be anonymous. They will simply open the app and be brought to the map where they will either add a ticket, or use it while they are driving. In the future safety checks will be added to make sure drivers aren’t using the app while driving and can only have the map view open allowing them to see the trends on the maps, being a complete hands free device. Another future update could be voice notifications and voice inputs allowing people to add their tickets hands free.
